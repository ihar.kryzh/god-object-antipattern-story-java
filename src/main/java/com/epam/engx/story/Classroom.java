package com.epam.engx.story;

import java.util.ArrayList;
import java.util.List;

public class Classroom {
  private final List<String> equipments = new ArrayList<>();;

  public void addEquipment(String equipment) {
    this.equipments.add(equipment);
  }
}
