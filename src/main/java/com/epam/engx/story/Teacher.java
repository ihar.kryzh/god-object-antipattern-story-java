package com.epam.engx.story;

public class Teacher {
  private String name;
  private int age;

  public Teacher(String name, int age) {
    this.name = name;
    this.age = age;
  }
}
