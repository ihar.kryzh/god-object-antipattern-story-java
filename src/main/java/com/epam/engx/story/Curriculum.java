package com.epam.engx.story;

import java.util.ArrayList;
import java.util.List;

public class Curriculum {
  private final List<String> subjects = new ArrayList<>();

  public void addSubject(String subject) {
    this.subjects.add(subject);
  }
}
