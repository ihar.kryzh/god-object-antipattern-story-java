package com.epam.engx.story;

import java.util.ArrayList;
import java.util.List;

public class Attendance {
  private final List<String> records = new ArrayList<>();

  public void addRecord(String record) {
    this.records.add(record);
  }
}
