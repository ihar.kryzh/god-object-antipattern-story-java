package com.epam.engx.story;

import java.util.ArrayList;
import java.util.List;

public class Assignment {
  private final List<String> assignments = new ArrayList<>();

  public void addAssignment(String assignment) {
    this.assignments.add(assignment);
  }
}
